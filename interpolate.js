var Files = Java.type("java.nio.file.Files")
var Paths = Java.type("java.nio.file.Paths")
var soo = Java.type("java.nio.file.StandardOpenOption")
var List = Java.type("java.util.List")
var IS = Java.type("java.util.stream.IntStream")

var in1 = arguments[0];
var in2 = arguments[1];
var out = arguments[2] || null;

var newList = function () [];
var addToList = function (l,e) l.push(e);
var doNothing = function (a,b) {};

var inLines1 = Files.lines(Paths.get(in1)).collect(newList, addToList, doNothing);
var inLines2 = Files.lines(Paths.get(in2)).collect(newList, addToList, doNothing);
var outLines = IS.range(0, inLines1.length > inLines2.length ? inLines1.length : inLines2.length)
                 .mapToObj(function (i) [inLines1[i],inLines2[i]])
                 .collect(newList, function (l,e) { if (e[0]) l.push(e[0]); if (e[1]) l.push(e[1]); }, doNothing);

if (out) {
    Files.write(Paths.get(out), 
        Java.to(outLines, List),
        soo.CREATE, soo.WRITE);
} else {
    for each (var line in outLines) {
        print(line);
    }
}
